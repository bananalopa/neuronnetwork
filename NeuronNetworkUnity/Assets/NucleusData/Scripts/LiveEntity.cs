﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]

public class LiveEntity : MonoBehaviour
{ 
    [SerializeField] private int health = 10;   // entity health
    [SerializeField] private int damageFromEnemies = 2;    // damage the enemies make to Entity
    [SerializeField] private int damageToEnemies = 2;
    [SerializeField] private float immuneTimeout = 0.1f; // time entity is immune for damage and pushing after last hit
    [SerializeField] private LayerMask whatIsGround = -1;   // mask determining what is ground to the character
    [SerializeField] private float groundedRadius = .2f; // radius of the overlap circle to determine if grounded
    [SerializeField] private float deathSpinMin = -100f;    // value to give the minimum amount of Torque when dying
    [SerializeField] private float deathSpinMax = 100f; // value to give the maximum amount of Torque when dying
    [SerializeField] private float pushBackForce = 300f; // force with whom entity push back on collision with enemy

    private bool dead;                 // entity death flag
    private bool immune; // entity is in immune state
    private bool grounded;            // whether or not the player is grounded.
    private bool facedToLeft;           // flag if entity flipped
    private Animator animator; // animator ref
    protected SkeletonAnimator sAnimator; // skeleton animator ref
    private float startImmuneTime; // tmp variable for immune time calculation
    private Transform groundCheck;    // position marking where to check if the player is grounded.
    private Rigidbody2D rg; // rigidbody ref

    public bool Immune
    {
        get { return immune; }
    }

    public bool Grounded
    {
        get { return grounded; }
    }

    public bool Dead
    {
        get { return dead; }
    }

    public int Health
    {
        get { return health; }
    }

    public bool FacedToLeft
    {
        get { return facedToLeft; }
    }

    public LayerMask WhatIsGround
    {
        get { return whatIsGround; }
    }

    void OnEnable()
    {
        groundCheck = transform.Find("GroundCheck");
        rg = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sAnimator = GetComponent<SkeletonAnimator>();
    }

    void FixedUpdate()
    {
        if (health <= 0 && !dead)
            Death();
        if (immune && Time.time > startImmuneTime + immuneTimeout)
            immune = false;
        grounded = StaticMethods.GroundCheck(gameObject, groundCheck, groundedRadius, whatIsGround);
        animator.SetBool("grounded", grounded);
    }

    public void PushBack(Transform other) 
    {
        rg.AddForce((rg.transform.position - other.position) * pushBackForce);
    }

    public void ReceiveDamage(int damage)
    {
        if (!immune)
            health -= damage;
        StartImmune();
    }

    private void StartImmune()
    {
        immune = true;
        startImmuneTime = Time.time;
    }


    private void Death()
    {
        if (dead)
            return;
        foreach (var v in GetComponentsInChildren<Collider2D>())
            v.enabled = false;
        rg.fixedAngle = false;
        rg.AddTorque(Random.Range(deathSpinMin, deathSpinMax));
        animator.SetBool("dead", true);
        dead = true;
    }

    private void Flip(SkeletonAnimator sAnimator)
    {
        sAnimator.skeleton.flipX = !sAnimator.skeleton.FlipX;
        if (sAnimator.skeleton.flipX)
            facedToLeft = true;
        else
            facedToLeft = false;
    }


}

﻿using UnityEngine;
using System.Collections.Generic;

public class StaticMethods : MonoBehaviour 
{
    private StaticMethods() { }
    private static StaticMethods instance =  null;
    public static StaticMethods Instance
    {
        get
        {
            if (instance == null)
                instance = (StaticMethods)FindObjectOfType(typeof(StaticMethods));
            return instance;
        }
    }

    [SerializeField] private List<string> rootTags;
    public List<string> RootTags
    {
       get{return rootTags;}
    }

    public static Transform FindParentWithTag(Transform t, List<string> tags)
    {
        if (tags.Contains(t.tag))
            return t;
        while (t.parent != null)
        {
            if (tags.Contains(t.parent.tag))
            {
                return t.parent;
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }

    public static Transform FindParentWithTag(Transform t, string tag)
    {
        if (tag == t.tag)
            return t;
        while (t.parent != null)
        {
            if (tag == t.tag)
            {
                return t.parent;
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }


    public static bool GroundCheck(GameObject gameObject, Transform groundCheck, float groundedRadius, LayerMask whatIsGround)
    {
        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (!colliders[i].transform.IsChildOf(gameObject.transform))
                return true;
        }
        return false;
    }
}

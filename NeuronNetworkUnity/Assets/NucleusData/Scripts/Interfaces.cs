﻿using UnityEngine;
using System.Collections;


interface ILiveEntity<T>
{
    bool Dead{ get; }
    T Health { get; }
    bool Grounded { get;}
    bool Immune { get; }
    bool FacedToLeft { get; }

    void ReceiveDamage(T damage);
    void PushBack();


}






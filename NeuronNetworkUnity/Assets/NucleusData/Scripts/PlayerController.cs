using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(LiveEntity))] 
public class PlayerController  : MonoBehaviour
{
    private PlayerController() { } //constructor for only 1-way singltone creation
    private static PlayerController instance = null; //singleton instance is null cause with will catch unity precreated instance in get method
    public static PlayerController Instance
    {
        get 
        {
            if (instance == null)
                instance = (PlayerController)FindObjectOfType(typeof(PlayerController));
            return instance; 
        }
    }
    //

    [SerializeField] private float maxSpeed = 10f;                    // The fastest the player can travel in the x axis.
    [SerializeField] private float jumpForce = 4000f;                  // Amount of force added when the player jumpFlags.
    [Range(0, 1)] [SerializeField] private float crouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [SerializeField] private bool airControl;                 // Whether or not a player can steer while jumpFlaging;
    [SerializeField] private bool superEFlag;   // superE state
    [Range(-1, 1)] [SerializeField] private int charge; 		        // current player charge, controlled by animator events
    [SerializeField] private bool block; 			// current player block state, controlled by animator events
    [SerializeField] private bool attack; 			// current player attack state, controlled by animator events
    [SerializeField] private Transform radFormDebrisEffectPrefabSparks;
    [SerializeField] private Transform radFormDebrisEffectPrefabFull;
    [SerializeField] private Transform eFormAuraEffectPrefab;
    [SerializeField] private Transform pFormAuraEffectPrefab; 

    private LiveEntity liveProps;
    private Animator animator;
    private SkeletonAnimator sAnimator; 
    private Rigidbody2D rg;
    private bool facingRight = true;  // For determining which way the player is currently facing.
    private bool jumpFlag;
    private ChargedColliderDamageReceiver mainDamageReceiver; // main damage receiver collider
    private ChargedColliderDamageReceiver pPunchCollider;
    private ChargedColliderDamageReceiver ePunchCollider;
    private Transform groundCheck;    // position marking where to check if the player is grounded.
    private AudioSource audioStep;
    private AudioSource audioJump;
    private AudioSource audioToETransform;
    private AudioSource audioEFormPulse;
    private AudioSource audioPPunch;
    private AudioSource audioESlice;
    private AudioSource audioEShield;
    private AudioSource audioPBlock;

    [SerializeField]
    private AudioClip clipStep;
    [SerializeField]
    private AudioClip clipJump;
    [SerializeField]
    private AudioClip clipToETransform;
    [SerializeField]
    private AudioClip clipEFormPulse;
    [SerializeField]
    private AudioClip clipPPunch;
    [SerializeField]
    private AudioClip clipESlice;
    [SerializeField]
    private AudioClip clipEShield;
    [SerializeField]
    private AudioClip clipPBlock;

    private void OnEnable()
    {
        mainDamageReceiver = transform.Find("mainDamageReceiver").GetComponent<ChargedColliderDamageReceiver>();
        pPunchCollider = transform.Find("pPunchCollider").GetComponent<ChargedColliderDamageReceiver>();
        ePunchCollider = transform.Find("ePunchCollider").GetComponent<ChargedColliderDamageReceiver>();
        rg = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sAnimator = GetComponent<SkeletonAnimator>();
        liveProps = GetComponent<LiveEntity>();
        groundCheck = transform.Find("GroundCheck");

        audioStep = AddAudio(clipStep, false, false, 1f);
        audioJump = AddAudio(clipJump, false, false, 1f);
        audioToETransform = AddAudio(clipToETransform, false, false, 1f);
        audioEFormPulse = AddAudio(clipEFormPulse, true, false, 1f);
        audioPPunch = AddAudio(clipPPunch, false, false, 1f);
        audioESlice = AddAudio(clipESlice, false, false, 1f);
        audioEShield = AddAudio(clipEShield, false, false, 1f);
        audioPBlock = AddAudio(clipPBlock, false, false, 1f);
    }


    private void Update()
    {
        InputManager();
    }


    private void FixedUpdate()
    {
		animator.SetInteger ("charge", charge); //update animator charge value
        bool crouch = Input.GetKey(KeyCode.LeftControl);
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        Move(h, crouch);

        // If the player should jumpFlag...
        if (liveProps.Grounded && jumpFlag)
        {
            // Add a vertical force to the player.
            animator.SetBool("jump", true);
            rg.AddForce(new Vector2(0f, jumpForce));
        }

        jumpFlag = false;

        if ((animator.GetFloat("idleWalkRunBlend")>0f)&&(liveProps.Grounded)&&(animator.GetBool("superE")))
            Instantiate(radFormDebrisEffectPrefabFull,groundCheck.position,groundCheck.rotation);
        if ((animator.GetFloat("idleWalkRunBlend") == 0f) && (liveProps.Grounded) && (animator.GetBool("superE")))
            Instantiate(radFormDebrisEffectPrefabSparks, groundCheck.position, groundCheck.rotation);
        if (charge==-1)
        {
            eFormAuraEffectPrefab.gameObject.SetActive(true);
            pFormAuraEffectPrefab.gameObject.SetActive(false);
        }
        if (charge == 1)
        {
            eFormAuraEffectPrefab.gameObject.SetActive(false);
            pFormAuraEffectPrefab.gameObject.SetActive(true);
        }
    }

    public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    {
        AudioSource newAudio = gameObject.AddComponent<AudioSource>();
        newAudio.clip = clip;
        newAudio.loop = loop;
        newAudio.playOnAwake = playAwake;
        newAudio.volume = vol;
        return newAudio;
    }

    private void Move(float move, bool crouch)
    {
        if (liveProps.Immune)
            return;
        if (liveProps.Grounded || airControl)
        {
            move = (crouch ? move * crouchSpeed : move);
            animator.SetFloat("idleWalkRunBlend", Mathf.Abs(move));
            rg.velocity = new Vector2(move * maxSpeed, rg.velocity.y);
            if (move > 0 && !facingRight)
            {
                Flip(sAnimator);
                facingRight = !facingRight;
            }
            else if (move < 0 && facingRight)
            {
                Flip(sAnimator);
                facingRight = !facingRight;
            }
        }
    }

    private void InputManager()
    {
        if ((!jumpFlag) && (liveProps.Grounded))
        {
            jumpFlag = CrossPlatformInputManager.GetButtonDown("Jump");
            animator.SetBool("jump", jumpFlag);
            if (jumpFlag)
            {
                audioJump.Stop();
                audioJump.Play();
            }
        }
        animator.SetBool("attack", CrossPlatformInputManager.GetButtonDown("Attack"));
        animator.SetBool("block", CrossPlatformInputManager.GetButton("Block"));
		if (CrossPlatformInputManager.GetButton ("ProtonState"))
            charge = 1;   			
		else
			charge = -1;
        mainDamageReceiver.Charge = charge;
        if (attack && (charge == 1))
            pPunchCollider.gameObject.SetActive(true);
        else
            pPunchCollider.gameObject.SetActive(false);
        if (attack && (charge == -1))
            ePunchCollider.gameObject.SetActive(true);
        else
            ePunchCollider.gameObject.SetActive(false);
        mainDamageReceiver.BlockPart = block;
        if (Input.GetKeyDown(KeyCode.E))
        {
            superEFlag = !superEFlag;
            if (superEFlag)
            {
                audioToETransform.Stop();
                audioToETransform.Play();
            }
            else
            {
                audioEFormPulse.Stop();
            }
            animator.SetBool("superE", superEFlag);

        }
    }

    protected void Flip(SkeletonAnimator sAnimator)
    {
        sAnimator.skeleton.flipX = !sAnimator.skeleton.FlipX;
    }

	void stepEvent()
	{
		audioStep.Stop();
		audioStep.Play();
	}

	void jumpTopEvent()
	{
		animator.SetBool("jump", false);
	}

	void swordHitEvent()
	{
		audioESlice.Stop();
		audioESlice.Play();
	}

	void punchEvent()
	{
		audioPPunch.Stop();
		audioPPunch.Play();
	}

	void eShieldAssembledEvent()
	{
		if (superEFlag)
		{
			audioEFormPulse.Stop();
			audioEFormPulse.Play();
		}
	}

	void nullStateEvent()
	{
		charge = 0;
	}

	void pStateEvent()
	{
		charge = 1;
	}

	void eStateEvent()
	{
		charge = -1;
	}

    void attackStartEvent()
    {
        attack = true;
    }
    
    void attackEndEvent()
    {
        attack = false;
    }

    void blockStartEvent()
    {
        block = true;
    }

    void blockEndEvent()
    {
        block = false;
    }
}


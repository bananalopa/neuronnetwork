﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LiveEntity))]

public class Enemy : MonoBehaviour
{
	[SerializeField] private float moveSpeed = 2f;  // speed the enemy moves at
    [SerializeField] private bool movable = false; // can entity move or not
    
    private Transform frontBound;   // reference to the position of the gameobject used for checking if something is in front
    private Animator animator; // animator reference
    private SkeletonAnimator sAnimator; // skeleton animator ref
	private Rigidbody2D rg; //rigidbody ref
    private LiveEntity liveProps; //

    void OnEnable()
    {
        // Setting up the references.
        liveProps = GetComponent<LiveEntity>();
        if (movable)
            frontBound = transform.Find("FrontBound").transform;
        animator = GetComponent<Animator>();
        sAnimator = GetComponent<SkeletonAnimator>();
        rg = GetComponent<Rigidbody2D>();
        //
    }

	void FixedUpdate ()
	{

        //move
        if (liveProps.Grounded)
            Move();
	}






    private void Move()
    {
        //move only entity is movable
        if ((!movable)||liveProps.Immune)
            return;
        // Create an array of all the colliders in front of the enemy.
        Collider2D[] frontHits = Physics2D.OverlapPointAll(frontBound.position, liveProps.WhatIsGround);
        // Check each of the colliders.
        foreach (Collider2D c in frontHits)
        {
            if (c.transform == frontBound.transform) // exclude own collider
                continue;
            Transform otherColliderRoot = StaticMethods.FindParentWithTag(c.transform, StaticMethods.Instance.RootTags);
            if (otherColliderRoot != null) //don't flip if collide with player
                if (otherColliderRoot.tag == "Player")
                    continue;
            // ... Flip the enemy and stop checking the other colliders.
            Flip(sAnimator);
            break;
            //
        }

        // Set the enemy's velocity to moveSpeed in the x direction.
        rg.velocity = new Vector2(transform.localScale.x * moveSpeed, rg.velocity.y);
    }

    protected void Flip(SkeletonAnimator sAnimator)
    {
        //
        sAnimator.skeleton.flipX = !sAnimator.skeleton.FlipX;
        moveSpeed = -moveSpeed;
    }
}

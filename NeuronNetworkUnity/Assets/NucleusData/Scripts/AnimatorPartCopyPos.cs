﻿using UnityEngine;
using System.Collections;

public class AnimatorPartCopyPos : MonoBehaviour {


    [SerializeField] private string boneName;
    [SerializeField] private bool copyZRotation;

    private Transform root;
    private SkeletonAnimator rootSAnimator;
    private LiveEntity rootLiveEntity;
    private bool flip;

	void OnEnable () 
    {
        root = StaticMethods.FindParentWithTag(transform, StaticMethods.Instance.RootTags);
        if (root == null)
            return;
        rootSAnimator = root.GetComponent<SkeletonAnimator>();
        rootLiveEntity = root.GetComponent<LiveEntity>();
	}
	
	void FixedUpdate ()
    {
        if (root == null)
            return;
        Vector3 newBonePos = new Vector3(rootSAnimator.skeleton.FindBone(boneName).worldX, rootSAnimator.skeleton.FindBone(boneName).worldY);
        transform.localPosition = newBonePos;

        if (copyZRotation)
        {
            Vector3 rot = transform.localRotation.eulerAngles;
            //Debug.Log(rootSAnimator.skeleton.FindBone(boneName).rotation);
            if (rootSAnimator.skeleton.FlipX)
                rot = new Vector3(rot.x, rot.y, 180f-rootSAnimator.skeleton.FindBone(boneName).worldRotation);  //WorldRotation); //rootSAnimator.skeleton.FindBone(boneName).worldRotation);
            else
                rot = new Vector3(rot.x, rot.y, rootSAnimator.skeleton.FindBone(boneName).worldRotation);  //WorldRotation); //rootSAnimator.skeleton.FindBone(boneName).worldRotation);
            transform.localRotation = Quaternion.Euler(rot);


            //Vector3 newBoneRot = new Vector3(rootSAnimator.skeleton.FindBone(boneName).worldRotation .worldX, rootSAnimator.skeleton.FindBone(boneName).worldY);

            //transform.localRotation = Quaternion.Euler
            //                            (transform.localRotation.ToEuler()., 
            //                            transform.localRotation.ToEuler().y, 
            //                            Quaternion.AngleAxis(rootSAnimator.skeleton.FindBone(boneName).worldRotation, Vector3.forward).ToEuler().z);
            //transform.localRotation = Quaternion.AngleAxis(rootSAnimator.skeleton.FindBone(boneName).worldRotation, Vector3.forward).eulerAngles + Quaternion.AngleAxis(270f, Vector3.right);
            
            //Vector3 tmp = new Vector3(transform.localRotation.eulerAngles.x,transform.localRotation.eulerAngles.y,
            //Quaternion.AngleAxis(rootSAnimator.skeleton.FindBone(boneName).worldRotation, Vector3.forward).eulerAngles.z);

            //transform.localRotation = tmp.
            //transform.localRotation.eulerAngles.x+transform.localRotation.eulerAngles.y

           // transform.localRotation.AngleAxis ( rootSAnimator.skeleton.FindBone(boneName).worldRotation, Vector3.forward);
        }
	}
}

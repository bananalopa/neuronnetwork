﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChargedColliderDamageReceiver : MonoBehaviour 
{
    [Range(-1,1)][SerializeField] private int charge; //Charge of collider (usually -1, 0 or +1)
    public int Charge
    {
        get { return charge; }
		set { charge = value;}
    }

    [SerializeField] private bool attackPart; // Does this part used for attack or not?
    public bool AttackPart 
    {
        get { return attackPart; }
        set { attackPart = value; }
    }

	[SerializeField] private bool blockPart; // Does this part used for block or not?
	public bool BlockPart 
	{
		get { return blockPart; }
        set { blockPart = value; }
	}


	private Transform entityRoot;
       

	void OnEnable () 
    {
        entityRoot = StaticMethods.FindParentWithTag(transform, StaticMethods.Instance.RootTags);
	}

    void OnCollisionEnter2D(Collision2D collision) 
    {
        Transform otherColliderRoot = StaticMethods.FindParentWithTag(collision.collider.transform,StaticMethods.Instance.RootTags);
        if (collision.collider.GetComponent<ChargedColliderDamageReceiver>()==null)
            return; //return if otherCollider has no charge

        // now work only for Player and Enemy

        if (!StaticMethods.Instance.RootTags.Contains(entityRoot.tag))
            return;

        int damageCalcValue;
        damageCalcValue = DamageCalc(transform.GetComponent<ChargedColliderDamageReceiver>(),
                                                            collision.collider.GetComponent<ChargedColliderDamageReceiver>());
        LiveEntity liveProps = entityRoot.GetComponent<LiveEntity>();
        
        if (damageCalcValue == -1)
            liveProps.PushBack(collision.transform);
        if (damageCalcValue == 1)
        {
            liveProps.ReceiveDamage(0);
            liveProps.PushBack(collision.transform);
        }

    }

    // this method make decision does Entity receive damage, push back or nothing happen
    // int values: 
    // -1 push entity back, 
    //  0 nothing happens, 
    //  1 entity receive damage
    private int DamageCalc(ChargedColliderDamageReceiver child, ChargedColliderDamageReceiver otherChild)
    {
        if (child.Charge == otherChild.Charge) // pretend charge cannot be 0
        {
            if ((child.AttackPart) || (!child.BlockPart))
                return -1;
        }
        if (child.Charge == (-1) * otherChild.Charge)
        {
            if (child.AttackPart && !otherChild.AttackPart)
                return 0;
            else
                return 1;
        }
        return 0;
    }

}
